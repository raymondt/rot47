# Application : rot47 cypher cli program
#	Description
# Author Raymond Thomson

all: help

CC=gcc
MAN=man/
SRC=src/
CFLAGS=-I.
LIBS=-lm

%.o: %.c
	@echo 'Invoking: GCC C object files'
	$(CC) -c -o $($SRC)$@ $< $(CFLAGS)

rot47: $(SRC)rot47.o
	@echo 'Building and linking target: $@'
	$(CC) -o rot47 $(SRC)*.o $(LIBS)

local: rot47 clean

clean:
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	@echo 'Installed. Enter ./rot47 to run'

install: rot47 
	@echo 'Installing'	
	cp rot47 /usr/local/bin/rot47
	cp $(MAN)rot47 /usr/share/man/man1/rot47.1
	gzip /usr/share/man/man1/rot47.1
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	rm -f rot47
	@echo 'installed, type rot47 to run or man rot47 for the manual'

remove:
	rm -f rot47

uninstall:
	rm -f /usr/local/bin/rot47
	rm -f /usr/share/man/man1/rot47.1.gz
	@echo 'rot47 uninstalled.'

help:
	@echo 'Make options for rot47'
	@echo 'Local Folder make'
	@echo '    make local'
	@echo 'Local Folder uninstall'
	@echo '    make remove'
	@echo 'System Install'
	@echo '    sudo make install'
	@echo 'System Uninstall'
	@echo '    sudo make uninstall'














	
