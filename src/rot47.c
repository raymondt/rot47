/*
 * 	FILE: 			rot13.c
 *  CREATED: 		16 July 2019
 *  AUTHOR: 		Raymond Thomson
 *  CONTACT: 		raymond.thomson76@gmail.com
 *  COPYRIGHT:		Free Use
 */

#define chstart (int)'!'
#define chstop (int)'~'

#include <stdio.h>
#include <unistd.h>

int main (void) {
	char ch;
	char buf[BUFSIZ];
	int tc;

	while ((tc = read(0, buf, BUFSIZ)) > 0) {
		for (int i = 0;i < tc;i++) {
			ch = buf[i];
			if (ch >= chstart && ch <= chstop) ch = (((ch-chstart)+47)%94)+chstart;
			buf[i] = ch;
		}
		write(1, buf, tc);
	}
	return 0;
}
